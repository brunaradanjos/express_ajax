var express = require('express');
var router = express.Router();
var connMysql = require('../config/dbConn.js')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
});

router.get('/listar_usuario', function (req, res, next) {
  connMysql.query('select * from usuarios', function (err, result) {
    console.log('dados usuarios: ', result);
    res.render('listar_usuario', { data: result });
  })
});

router.get('/cadastrar_usuario', function (req, res, next) {
  res.render('cadastrar_usuario');
});

router.get('/editar_usuario', function (req, res, next) {
  console.log('parametro: ', Object.keys(req.query).toString());
  var query = Object.keys(req.query).toString()
  connMysql.query('select * from usuarios where id =' + query, function (err, result) {
    res.render('editar_usuario', { data: result[0] });
  })
});

router.get('/sobre', function (req, res, next) {
  res.render('sobre', { title: 'Express' });
});
//salvar
router.post('/salvar_usuario', function (req, res, next) {
  var { nome, telefone, email, sexo } = req.body
  var error = []

  if (nome === '') {
    console.log('nome não')
    error.push('Campo nome não pode ser vazio')
  }
  if (telefone === '') {
    console.log('nome não')
    error.push('Campo telefone não pode ser vazio')
  }
  if (email === '') {
    console.log('nome não')
    error.push('Campo email não pode ser vazio')
  }
  if (sexo === '') {
    console.log('nome não')
    error.push('Campo sexo não pode ser vazio')
  }

  console.log('dados do erro ', error)


  if (nome !== '' && telefone !== '' && email !== '' && sexo !== '') {
    let data = {
      nome: nome,
      telefone: telefone,
      email: email,
      sexo: sexo
    }
    connMysql.query('insert into usuarios set ?', data)
    res.send({ result: data });
  } else {
    res.send({ result: error })
  }

});
// Deletar
router.delete('/deletar_usuario', function (req, res, next) {
  var data = req.url
  console.log('dados recebidos ', data);
  connMysql.query('delete from usuarios where id = ' +
    data.replace("/deletar_usuario?", ""))

  res.send({ result: data });
});
// Atualizar
router.put('/editar_usuario', function (req, res, next) {
  var data = req.body
  console.log('corpo ', data.id);
  var { nome, telefone, email, sexo } = data
  var editar = {
    nome,
    telefone,
    email,
    sexo
  }
  connMysql.query('update usuarios set ' + (`nome = '${data.nome}', telefone = '${data.telefone}', email = '${data.email}', sexo = '${data.sexo}'`) + ' where id =' + data.id)

  res.send({ result: data });
});

module.exports = router;
